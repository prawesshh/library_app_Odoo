{
'name': 'Library Management Sys',
'description': 'Manage library book catalogue and lending.',
'author': 'Prawesh Panthi',
'depends': ['base'],
'application': True,
'data': [
'security/library_security.xml',
'security/ir.model.access.csv',
'views/library_menu.xml',
'views/book_view.xml',
],
}
